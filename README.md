# Laravel+Sanctum+Vue+Bootstrap - MPA Boilerplate
### This Boilerplate has:
* Laravel 9 + Sanctum
* Vue 3
* Bootstrap 5
* Login, Register, ... Views

### How to use this template:
* `git clone ... myapp`
* `cd myapp`
* `edit .env`
* `make init`
* `make up` OR `make down`
* `make sh` - for work with backend: composer and artisan
* `cd data/back` - for work with frontend: npm, vite



## How is it prepared

#### Install Laravel UI
- `composer require laravel/ui`
#### Install scaffolding
- `php artisan ui bootstrap --auth`
#### Migrate
- `php artisan migrate`

#### Install Vue
- `npm i vue@next @vitejs/plugin-vue --save-dev`
#### Install and Run npm
- `npm install && npm run dev`


#### Files changed

vite.config.js
```javascript
import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import vue from '@vitejs/plugin-vue';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/sass/app.scss',
                'resources/js/app.js',
            ],
            refresh: true,
        }),
        vue({
            template: {
                transformAssetUrls: {
                    base: null,
                    includeAbsolute: false,
                },
            },
        }),
    ],
    resolve: {
        alias: {
            vue: 'vue/dist/vue.esm-bundler.js',
        },
    },
});
```


resources/js/app.js
```javascript
import './bootstrap';
import { createApp } from 'vue';
const app = createApp({});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./views/ExampleComponent.vue -> <example-component></example-component>
 */
Object.entries(import.meta.glob('./**/*.vue', { eager: true })).forEach(([path, definition]) => {
    app.component(path.split('/').pop().replace(/\.\w+$/, ''), definition.default);
});

app.mount('#app');
```


resources/views/layouts/app.blade.php
```html
...
<body id="app">
...
```