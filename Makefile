include .env

# Pass parameters to docker-compose
CURRENT_USER := $(shell id -un)
CURRENT_USERID := $(shell id -u)
export CURRENT_USER
export CURRENT_USERID

help:
	@echo ""
	@echo "Config File: .env"
	@echo ""
	@echo "Usage:"
	@echo "make init       - Configure Services, run this only first time"
	@echo "make start      - Start Services"
	@echo "make stop       - Stop Services"
	@echo "make up         - Create and Start Services"
	@echo "make down       - Stop and Delete Services"
	@echo "make restart    - Restart Server"
	@echo "make sh         - Interactive mode on php image"
	@echo "make build      - Build Container image"


# For Development
init:
	@# Change nginx.conf file
	@sed -i "s/server_name .*/server_name ${HOST};/" docker/nginx/default.conf

	@if [ -d ${DB_PATH} ]; then \
		read -p "Remove DB folder? [y/N] " ans && ans=$${ans:-N}; \
		if [ $${ans} = y ] || [ $${ans} = Y ]; then \
		  	printf $(_SUCCESS) "Remove" ; \
			sudo rm -rf ${DB_PATH} ; \
		else \
			printf $(_SUCCESS) "NO" ; \
		fi \
	fi

	@if [ -d ${APP_PATH} ]; then \
		read -p "Remove ${APP_PATH} folder? [y/N] " ans && ans=$${ans:-N}; \
		if [ $${ans} = y ] || [ $${ans} = Y ]; then \
		  	printf $(_SUCCESS) "Remove" ; \
			sudo rm -rf ${APP_PATH} ; \
		else \
		  printf $(_SUCCESS) "NO" ; \
		fi \
	fi

	@mkdir -p ${APP_PATH}
	@# Change owner, group 82 is www-data on docker
	@sudo chown -R ${USER}:82 ${APP_PATH}

	@docker compose -f docker-compose-dev.yml up -d

	@echo ""
	@if [ -d ${APP_PATH}/app ]; then \
		read -p "Install Laravel? [y/N] " INST_LARA && INST_LARA=$${INST_LARA:-N} ; \
	else \
		INST_LARA=Y ; \
	fi ; \
	if [ $${INST_LARA} = y ] || [ $${INST_LARA} = Y ]; then \
		printf $(_SUCCESS) "YES" "Installing Laravel..." ; \
		docker exec ${APP_SLUG} composer create-project laravel/laravel . ; \
		sed -i "s/APP_NAME=.*/APP_NAME=${APP_NAME}/" ${APP_ENV_FILE} ; \
		sed -i "s/APP_ENV=.*/APP_ENV=local/" ${APP_ENV_FILE} ; \
		sed -i "s/DB_CONNECTION=.*/DB_CONNECTION=${DB_CONNECTION}/" ${APP_ENV_FILE} ; \
		sed -i "s/DB_HOST=.*/DB_HOST=${DB_HOST}/" ${APP_ENV_FILE} ; \
		sed -i "s/DB_PORT=.*/DB_PORT=${DB_PORT}/" ${APP_ENV_FILE} ; \
		sed -i "s/DB_DATABASE=.*/DB_DATABASE=${DB_DATABASE}/" ${APP_ENV_FILE} ; \
		sed -i "s/DB_USERNAME=.*/DB_USERNAME=${DB_USERNAME}/" ${APP_ENV_FILE} ; \
		sed -i "s/DB_PASSWORD=.*/DB_PASSWORD=${DB_PASSWORD}/" ${APP_ENV_FILE} ; \
		sed -i "s/APP_HOST=.*/APP_HOST=${HOST}/" ${APP_ENV_FILE} ; \
		sed -i "s/APP_URL=.*/APP_URL=http:\/\/${HOST}/" ${APP_ENV_FILE} ; \
		sed -i "s/REDIS_HOST=.*/REDIS_HOST=redis/" ${APP_ENV_FILE} ; \
		sudo chown -R ${USER}:82 ${APP_PATH} ; \
	else \
		printf $(_SUCCESS) "NO" "Finish." ; \
	fi

	@if [ -n "$$(grep -P ${HOST} /etc/hosts)" ]; then \
		echo "Site ${HOST} already exists in hosts" ; \
	else \
		echo "Adding site ${HOST} to your hosts" ; \
		sudo echo "127.0.0.1 ${HOST}" >> /etc/hosts ; \
    fi

	@echo ""
	@printf $(_SUCCESS) "Web ready:" "http://${HOST}:${HOST_PORT}"
	@echo ""

# For Development
start:
	@docker compose -f docker-compose-dev.yml start
	@printf $(_SUCCESS) "Web ready:" "http://${HOST}:${HOST_PORT}"

stop:
	@docker compose -f docker-compose-dev.yml stop

sh:
	@docker exec -it ${APP_SLUG} sh

up:
	@docker compose -f docker-compose-dev.yml up -d
	@printf $(_SUCCESS) "Web ready:" "http://${HOST}:${HOST_PORT}"

down:
	@docker compose -f docker-compose-dev.yml down

restart:
	@docker compose -f docker-compose-dev.yml down
	@docker compose -f docker-compose-dev.yml up -d

# For production
build:
	sudo chown -R ${USER} ${DB_PATH}
	docker compose -f docker-compose.yml -p ${APP_SLUG} build
	#docker compose -f docker-compose.yml up -d
	#docker compose -f docker-compose.yml down


_WARN := "\033[33m[%s]\033[0m %s\n"  # Yellow text for "printf"
_SUCCESS := "\033[32m[%s]\033[0m %s\n" # Green text for "printf"
_ERROR := "\033[31m[%s]\033[0m %s\n" # Red text for "printf"