import './bootstrap';
import { createApp } from 'vue';
const app = createApp({});

// import Page1 from './views/page1.vue';
// app.component('page1', Page1);
// import Page2 from './views/page2.vue';
// app.component('page2', Page2);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./views/ExampleComponent.vue -> <example-component></example-component>
 */
Object.entries(import.meta.glob('./**/*.vue', { eager: true })).forEach(([path, definition]) => {
    app.component(path.split('/').pop().replace(/\.\w+$/, ''), definition.default);
});

app.mount('#app');
